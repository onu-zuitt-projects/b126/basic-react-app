import React from 'react';        // Creating React components and stuff
import ReactDOM from 'react-dom'; // DOM manipulation

const BookList = () => {
  return (
    <>
      <Book
        image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1367545443l/157993.jpg"
        alt="The Little Prince book."
        title="The Little Prince"
        author="Antoine de Saint-Exupery"
      />
      <Book
        image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1166859639l/18135._SY475_.jpg"
        alt="Romeo and Juliet book."
        title="Romeo and Juliet"
        author="William Shakespear"
      />
      <Book
        image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1490528560l/4671._SY475_.jpg"
        alt="The Great Gatsby book."
        title="The Great Gatsby"
        author="F. Scott Fitzgerald"
      />
    </>
  );
};

const Book = ({ image, alt, author, title }) => {
  // console.log(props);
  return (
    <>
      <Image
        image={image}
        alt={alt}
      />
      <Title
        title={title}
      />
      <Author
        author={author}
      />
    </>
  );
};

const Image = ({ image, alt }) => {
  // console.log(props);
  return <img src={image} alt={alt} />;
};

const Title = ({ title }) => {
  // console.log(props);
  return <h3>{title}</h3>;
};

const Author = ({ author }) => {
  // console.log(props);
  return <h5>{author}</h5>;
};

ReactDOM.render(<BookList />, document.getElementById('root'));
